package com.gitlab.nicolaus1990.Tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

public class PropertyFileTools {

    private static Logger logger =  LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static Properties loadPropertiesFile(String propertiesPath)
    {

        Properties props = new Properties();
        InputStream input = null;

        try
        {
            input = new FileInputStream(propertiesPath);
            // load a properties file
            props.load(input);
        }
        catch (IOException e)
        {
            logger.error("Error while loading property file.");
            e.printStackTrace();
        }
        finally
        {
            if (input != null)
            {
                try
                {
                    input.close();
                }
                catch (IOException e)
                {
                    logger.error("Error while closing input stream of property file.");
                    e.printStackTrace();
                }
            }
        }
        return props;
    }

    public static String getOrEmptyStr(Properties props, String key)
    {
        if (props.containsKey(key))
        {
            String ans = (String) props.get(key);
            //msgProp.remove(key);//Will remove key to check after creating message if some property was not used.
            return ans;
        }
        logger.error("Could not find value for key: " + key);
        return "";//DEFAULT VALUE
    }

    public static String[][] getPropertyKeysValue(Properties props)
    {
        String[][] ans = new String[props.size()][2];
        int index = 0;
        for (String key : props.stringPropertyNames())
        {
            ans[index][0] = key;
            ans[index][1] = PropertyFileTools.getOrEmptyStr(props, key);
            index++;
        }
        return ans;
    }

    public static void logPropertiesKeyVals(Properties props)
    {
        //Main.getLog().info("Not all properties were used.");
        String[][] keysValues = PropertyFileTools.getPropertyKeysValue(props);
        for (String[] keyVal : keysValues)
        {
            logger.debug("Property unused: Key: " + keyVal[0] + ", Value: " + keyVal[1]);
        }
    }
}

