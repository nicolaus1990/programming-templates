package com.gitlab.nicolaus1990.Tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.Socket;

public class NetworkTester {

    private static Logger logger =  LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());


    public static void start(String ip, int port){
        logger.info("Connecting with ip: " + ip + " and port: " + port);
        if (hostAvailabilityCheck(ip, port, true)){
            logger.info("Connection succeeded.");
        }
        else{
            logger.info("Connection failed.");
        }
    }

    public static boolean hostAvailabilityCheck(String server_address, int tcp_server_port, boolean printError) {
        try (Socket s = new Socket(server_address, tcp_server_port)) {
            return true;
        } catch (IOException ex) {
            /* eventually ignore */
            if (printError){
                logger.info("Failed to connect: " , ex);
            }
        }
        return false;
    }
}
