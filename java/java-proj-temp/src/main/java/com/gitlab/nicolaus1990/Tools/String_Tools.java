package com.gitlab.nicolaus1990.Tools;

import java.io.File;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class String_Tools {


    public static String join(List<String> ss, String sep){
        StringBuilder csvBuilder = new StringBuilder();
        for(String s : ss){
            csvBuilder.append(s);
            csvBuilder.append(sep);
        }
        String csv = csvBuilder.toString();
        csv = csv.substring(0, csv.length() - sep.length());
        return csv;
    }

    public static String join(List<String> ss){
        return join(ss, ",");
    }



    public static String repeat(Integer numTimes, String toRepeat){
        //Credit to https://stackoverflow.com/questions/1235179/simple-way-to-repeat-a-string-in-java
        return new String(new char[numTimes]).replace("\0", toRepeat);
    }


    public static String removeRegex(String input, String regex, String replacement){
        String output = "";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        int index=0;
        while (matcher.find()) {
            output += input.substring(index, matcher.start()) + replacement;
            index = matcher.end();
        }
        if (index < input.length()){
            output += input.substring(index);
        }
        return output;
    }

    public static void findAllMatches(List<String> output, String target, String regex, int groupNum){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(target);
        while (matcher.find())
        {
            output.add(matcher.group(groupNum));
        }
    }

    /*
     * Random methods taken from: https://stackoverflow.com/questions/20536566/creating-a-random-string-with-a-z-and-0-9-in-java
     */

    public static String getSaltString(int length) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }


//    public static String removeSegment(String input, String segment, String replacement){
//        return input.replace(segment, replacement);
//    }

}
