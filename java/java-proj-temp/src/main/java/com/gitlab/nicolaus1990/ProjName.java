package com.gitlab.nicolaus1990;

import Tools.ApacheCLITools;
import Tools.LoggingTools;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

/*
 * For example, call with:
 *      -D -l C:\Users\nmoeller\Desktop\ -a action-1
 */
public class ProjName {

    private static final String CMDLINE_SYNTAX = "-l <logFile> -a <action> [-H host -p port ...] ";
    //TODO: Get available actions from enum in Arguments.
    private static String AVAILABLE_ACTIONS = "Valid options for action: action-1, action-2,...";
    private static final String HELP_HEADER =
                    "Programm header\n" +
                    "Java-Programm (jre 1.7.*).\n" +
                    AVAILABLE_ACTIONS + "\n";
    private static final String HELP_FOOTER = "FOOTER_HERE";

    private static Logger logger;

    enum OptionsCLI implements ApacheCLITools.ApacheCLIOptions {
        help("h","help",false, "Print help", false),
        log("l","log-path",true, "Log path", true),
        debug("D","debug",false, "Debug mode", false),
        action("a","action",true, AVAILABLE_ACTIONS, true),
        number("n","number",true, "Number (depends on action taken)", false);
        public final String opt;
        public final String longOpt;
        public final boolean hasArg;
        public final String description;
        public final boolean required;
        OptionsCLI(String opt, String longOpt, boolean hasArg, String description, boolean required){
            this.opt=opt;
            this.longOpt=longOpt;
            this.hasArg=hasArg;
            this.description=description;
            this.required=required;
        }

        @Override
        public String getOpt() { return opt; }

        @Override
        public String getLongOpt() { return longOpt; }

        @Override
        public boolean getHasArg() { return hasArg; }

        @Override
        public String getDescription() { return description; }

        @Override
        public boolean getRequired() { return required; }
    }

    public void start(String[] args)
    {
        boolean helpOptionGiven = true;
        try {
            helpOptionGiven = ApacheCLITools.checkForHelp(args);
        } catch (ParseException e) {
            e.printStackTrace();
            giveHelpAndExit(1);
        }
        if (helpOptionGiven)
        {
            giveHelpAndExit(0);
        }
        else
        {
            CommandLine cml = null;
            try {
                cml = ApacheCLITools.parseOptions(args, OptionsCLI.values());
            } catch (ParseException e) {
                e.printStackTrace();
                giveHelpAndExit(1);
            }

            LoggingTools.initLogging(cml.getOptionValue("l"), cml.getOptionValue("a"), cml.hasOption("D"));
            logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
            logger.info("Starting ProjName. Running on Java " + System.getProperty("java.version"));

            Arguments ap = null;
            try {
                ap = Arguments.registerArguments(cml);
            } catch (IllegalArgumentException e){
                giveHelpAndExit(1);
            }

            delegate(ap);
            logger.info("Finished.");
        }
    }

    private void giveHelpAndExit(int exitcode){
        ApacheCLITools.printHelp(ProjName.OptionsCLI.values(), CMDLINE_SYNTAX, HELP_HEADER, HELP_FOOTER);
        System.exit(exitcode);
    }

    static void delegate(Arguments args)
    {
        switch (args.getActionStr())
        {
            case "action-1":
                logger.info("Modality 1: Bla bla bla");
                break;
            case "action-2":
                logger.info("Modality 2: yada yada yada");
                break;
            default:
                logger.error(args.getActionStr() + " is not a valid action.");
                System.exit(-1);
        }
    }
}
