package com.gitlab.nicolaus1990.Examples;


import havi.de.tools.HL7Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.net.*;
import java.util.*;
/* 
 * 
 * To use like:
 * 

    public void listen() {
        SimpleThreads runner = new SimpleThreads(writer, port, maxLenQueueListener, numListenerThreads);
        Thread server2ndMessage = new Thread(runner,"Server 2nd message.");
        server2ndMessage.start();
    }



 * This class was taken almost as is from: https://github.com/SaravananSubramanian/hl7/blob/master/HL7%20Programming%20using%20Java%20-%20Tutorial%20Series/HL7%20Programming%20using%20Java%20-%20A%20Short%20Tutorial/MLLPBasedHL7ThreadedServer.java
 * See: https://saravanansubramanian.com/hl72xjavaprogramming/
 * 
 */
public class SimpleThreads implements Runnable{

    private static Logger logger =  LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static HL7MessageWriter writer = null;

    private int maxConnections;
    private int numListenerThreads;
    private int listenPort;



    public SimpleThreads(HL7MessageWriter writer, int aListenPort, int maxConnections, int numListenerThreads) {
        listenPort = aListenPort;
        this.maxConnections = maxConnections;
        this.numListenerThreads = numListenerThreads;
        this.writer = writer;
    }

    public void acceptConnections() {
        logger.info("Accepting connections for 2nd message (starting server).");
        try {
            ServerSocket server = new ServerSocket(listenPort, maxConnections);
            Socket clientSocket = null;
            while (true) {
                clientSocket = server.accept();
                handleConnection(clientSocket);
            }
        } catch (BindException e) {
            logger.error("HandlerSecondMessage: Unable to bind to port " + listenPort,e);
        } catch (IOException e) {
            logger.error("HandlerSecondMessage: Unable to instantiate a ServerSocket on port: " + listenPort, e);
        }
    }

    protected void handleConnection(Socket connectionToHandle) {
        ConnectionHandler.processRequest(connectionToHandle);
    }

//    public static void main(String[] args) {
//        HandlerSecondMessage2 server = new HandlerSecondMessage2(1080, 3);
//        server.setUpConnectionHandlers();
//        server.acceptConnections();
//    }

    public void setUpConnectionHandlers() {
        logger.debug("Setting up connection handlers. " + numListenerThreads + " threads processing client sockets.");
        for (int i = 0; i < numListenerThreads; i++) {
            ConnectionHandler currentHandler = new ConnectionHandler();
            Thread handlerThread = new Thread(currentHandler, "Handler " + i);
            handlerThread.setDaemon(true);
            handlerThread.start();
        }
    }

    @Override
    public void run() {
        logger.info("Starting server for 2nd message (thread).");
        setUpConnectionHandlers();
        acceptConnections();
    }

    private static class ConnectionHandler implements Runnable {
        private Socket connection;
        private static List pool = new LinkedList();

        public ConnectionHandler() {
        }

        public void handleConnection() {
            try {
                logger.debug("Handling client at "
                        + connection.getInetAddress().getHostAddress()
                        + " on port " + connection.getPort());

                InputStream in = connection.getInputStream();
                OutputStream out = connection.getOutputStream();

                String parsedHL7Message = HL7Tools.getMessage(in);
                String buildAcknowledgmentMessage = HL7Tools.getSimpleAcknowledgementMessage(parsedHL7Message);
                out.write(buildAcknowledgmentMessage.getBytes(), 0, buildAcknowledgmentMessage.length());

                String docId = HL7Tools.getMessageComponent(parsedHL7Message,"TXA", 12, 1, true);
                writer.writeReceived2ndPort(parsedHL7Message, docId);
                writer.writeSent2ndPort(buildAcknowledgmentMessage, docId);

            } catch (ProtocolException e) {
                logger.error("Error with incoming message (hl7 protocol not followed).",e);
                e.printStackTrace();
            } catch (SocketException e) {
                logger.error("Error while reading from socket",e);
                e.printStackTrace();
            } catch (IOException e) {
                logger.error("Error with connection: ",e);
                e.printStackTrace();
            } finally {
                try {
                    connection.close();
                }
                catch (IOException e) {
                    String errorMessage = "Error while closing connection: " + e.getMessage();
                    logger.error(errorMessage, e);
                    //throw new RuntimeException(errorMessage);
                }
            }
            //thread continues
        }

        public static void processRequest(Socket requestToHandle) {
            synchronized (pool) {
                pool.add(pool.size(), requestToHandle);
                pool.notifyAll();
            }
        }

        /*
         * Having a pool of sockets, and synchronizing them is not necessary yet in our projects, since we're
         * dealing with one connection, but could be useful if several connections at the same time are being made.
         */
        public void run() {
            while (true) {
                synchronized (pool) {
                    while (pool.isEmpty()) {
                        try {
                            pool.wait();
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                    connection = (Socket) pool.remove(0);
                }
                handleConnection();
            }
        }
    }
}
