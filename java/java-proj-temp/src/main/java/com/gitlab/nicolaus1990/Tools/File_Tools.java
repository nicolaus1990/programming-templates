package com.gitlab.nicolaus1990.Tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

public class File_Tools {

    private static Logger logger =  LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static String getExtension(String pathToFile){
        String extension = "";

        int i = pathToFile.lastIndexOf('.');
        //NOTE: "/" and "\" for linux and windows
        int p = Math.max(pathToFile.lastIndexOf('/'), pathToFile.lastIndexOf('\\'));

        if (i > p) {
            return pathToFile.substring(i+1);
        }
        return "";
    }

    public static String replaceExtension(String pathToFile, String extensionReplacement){
        String extension = getExtension(pathToFile);
        if (extension.equals("")){
            return pathToFile + "." + extensionReplacement;
        }
        return String_Tools.removeRegex(pathToFile,  extension + "$", extensionReplacement);
    }

    public static String getFileName(String filename) {

        int i = filename.lastIndexOf(File.separator);
        //i is either -1 or >= 0
        return filename.substring(i+1);
    }

    /*
    * Will retrieve file in resource folder (even if program was called as a jar-file).
    * Note: In gradle, resource folder is in 'src/main/resources'
    * @param The name of the file (resource). Only name, and without path to it.
    */
    public File getResource(String fileName){
        return new File(
                getClass().getClassLoader().getResource(fileName).getFile()
        );
    }


    private static void msgToFile(byte[] parsedMessage, String outputFilename) throws IOException {

        OutputStream outputStream = null;
        try
        {
            File file = new File(outputFilename);

            // quick check to create the file before writing if it does not exist already
            if (!file.exists())
            {
                if(!file.createNewFile())
                {
                    logger.error("Error creating new File!");
                }
            }

            outputStream = new FileOutputStream(file);
            outputStream.write(parsedMessage);
            outputStream.flush();

            logger.info("Message serialized to file " + file + " successfully.");
        }  finally {
            if (outputStream != null)
            {
                outputStream.close();
            }
        }
    }






//    public static String doc2Base64(String filePath) throws IOException {
//        return doc2Base64(Paths.get(filePath));
//    }
//
//    public static String doc2Base64(Path filePath) throws IOException {
//        byte[] pdfFile = new byte[0];
//        try
//        {
//            pdfFile = Files.readAllBytes(filePath);
//        }
//        catch (UnsupportedEncodingException e)
//        {
//            logger.error("UnsupportedEncodingException!",e);
//            throw e;
//        }
//        catch (IOException e)
//        {
//            logger.error("IOException while converting doc to base64",e);
//            //throw e;
//            return "";
//        }
//
//        return new String(Base64.encodeBase64(pdfFile));
//    }
}
