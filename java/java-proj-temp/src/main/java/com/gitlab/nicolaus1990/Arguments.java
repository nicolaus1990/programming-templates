package com.gitlab.nicolaus1990;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;


/*
 * ArgumentParser parses arguments and validates them.
 */
public final class Arguments
{

    private static Logger logger =  LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static Arguments args;
    private CommandLine cml;

    //Action to take.
    private String action;

    private String loggingFolder;
    private boolean debugMode;

    public boolean isDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    public String getLoggingFolder() {
        return loggingFolder;
    }

    public void setLoggingFolder(String loggingFolder) {
        this.loggingFolder = loggingFolder;
    }

    public String getActionStr() { return action; }

    public void setAction(String action) {
        this.action = action;
    }

    enum ACTION {
        ACTION1,
        ACTION2,
        ERROR;
        public static ACTION getAction(String actionStr){
            for (ACTION a : ACTION.values()){
                if (a.name().toLowerCase().equals(actionStr)){
                    return a;
                }
            }
            return ERROR;
        }
    }



    public static Arguments registerArguments(CommandLine cml)
    {
        Arguments ap = new Arguments();
        //Required arguments:
        ap.setAction(cml.getOptionValue("a"));
        ap.setLoggingFolder(cml.getOptionValue("l"));
        //Required arguments accorind to "action":
        ACTION act= ACTION.getAction(ap.getActionStr());
        switch (act)
        {
            case ACTION1:
                if (cml.hasOption("H"))
                {

                }
                else
                {
                    logger.error("Missing arguments for action1. Required arguments: ....");
                    throw new IllegalArgumentException("Missing arguments.");
                }
                break;

            case ACTION2:
                if (cml.hasOption("P"))
                {

                }
                else
                {
                    logger.error("Missing arguments for action-1. Required arguments: ...");
                    throw new IllegalArgumentException("Missing arguments.");
                }
                break;
            default:
                logger.error("Action " + ap.getActionStr() + " not supported");
                throw new IllegalArgumentException("Missing arguments.");
        }
        return ap;
    }
}
