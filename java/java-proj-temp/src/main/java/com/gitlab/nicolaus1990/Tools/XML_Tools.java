package Tools;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.*;
import java.io.*;
import java.net.URL;

public class XML_Tools {


    public static Document parseDoc(File file){

        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder(); // ParserConfigurationException

            Document doc = dBuilder.parse(file); // SAXException, IOException
            //doc.getDocumentElement().normalize();
            // com.sun.org.apache.xerces.internal.dom.DeferredDocumentImpl

            return doc;
        }
        catch (ParserConfigurationException | IOException | org.xml.sax.SAXException ex)
        {
            //ex.printStackTrace();
            throw new RuntimeException(ex.getLocalizedMessage());
        }
    }

    public static NodeList xpathSearch(Node node, String xPathExpr){
        try {
            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath xPath = xPathFactory.newXPath();

            XPathExpression expr = xPath.compile(xPathExpr);

            NodeList result = (NodeList) expr.evaluate(node, XPathConstants.NODESET);
            //Object result = expr.evaluate(doc, XPathConstants.NODESET);
            return result;
        } catch (XPathExpressionException e) {
            //e.printStackTrace();
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    public static NodeList parseDocAndXPathSearch(File file, String xPathExpr){
        Document doc = parseDoc(file);
        return xpathSearch(doc, xPathExpr);
    }

//    public NodeList xpathAllNodesFromNode(){
//
//        try {
//            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
//            //domFactory.setNamespaceAware(true);
//            DocumentBuilder builder = null;
//            builder = domFactory.newDocumentBuilder();
//
//            Document doc = builder.parse("src/main/java/books.xml");
//
//            XPathFactory factory = XPathFactory.newInstance();
//            XPath xpath = factory.newXPath();
//
//            Node book = (Node) xpath.evaluate("//book[author='Neal Stephenson']", doc, XPathConstants.NODE);
//            Node title = (Node) xpath.evaluate("/title", book, XPathConstants.NODE); // I get null here.
//            Node inventory = (Node) xpath.evaluate("/inventory", book, XPathConstants.NODE); // this returns a node.
//        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
//            e.printStackTrace();
//        }
//
//    }


    /*
    * Validates xml against schema.
    * @param xsdName: Filename of schema, that is contained in resource folder. Just filename and not a path.
    * @param
    */
    public boolean validateXMLagainstXSD(String xmlPath, String xsdName) {
        File xsdFile = new File(
                getClass().getClassLoader().getResource(xsdName).getFile()
        );
        return validateXMLagainstXSD(xmlPath,xsdFile);
    }

    public boolean validateXMLagainstXSD(String xmlPath, File xsdFile) {


        URL xsdURI = getClass().getClassLoader().getResource(xsdFile.getName());

        InputStream in = getClass().getResourceAsStream(xsdFile.getName());
        BufferedReader streamXSD = new BufferedReader(new InputStreamReader(in));



        //This code snippet was taken from: https://stackoverflow.com/questions/15732/whats-the-best-way-to-validate-an-xml-file-against-an-xsd-file
        try {
            // parse an XML document into a DOM tree
            DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = parser.parse(new File(xmlPath));

            // create a SchemaFactory capable of understanding WXS schemas
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Source xmlFile = new StreamSource(new File(xmlPath));
            Schema schema = factory.newSchema(xsdURI);

            // create a Validator instance, which can be used to validate an instance document
            Validator validator = schema.newValidator();

            try {
                //validator.validate(new DOMSource(document));
                validator.validate(xmlFile);
            } catch (org.xml.sax.SAXException e) {
                throw new IllegalArgumentException("XML " + xmlPath + " was rejected by XSD-file.");
            }


        }  catch (IOException e){
            throw new IllegalArgumentException("IOError while validating XML.");
        } catch (ParserConfigurationException e) {
            throw new IllegalArgumentException("ParserConfigurationException while validating XML.");
        } catch (SAXException e) {
            throw new IllegalArgumentException("SAXException while validating XML.");
        }
        return true;
    }

}
