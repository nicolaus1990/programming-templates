package com.gitlab.nicolaus1990.Tools;

import org.apache.commons.cli.*;
/*
 * This class has helpful static methods to help with apache commons cli package.
 */
public class ApacheCLITools {

    /*
     * This interface can be implemented by an enum.
     * Each instance of an enum can be retrieved with ClassName.EnumName.values()
     */
    public interface ApacheCLIOptions {
        public String getOpt();
        public String getLongOpt();
        public boolean getHasArg();
        public String getDescription();
        public boolean getRequired();
    }

    private static Options allOptions(ApacheCLIOptions[] clios){
        Options options = new Options();
        for (ApacheCLIOptions clio : clios){
            Option o = new Option(clio.getOpt(), clio.getLongOpt(), clio.getHasArg(), clio.getDescription());
            o.setRequired(clio.getRequired());
            options.addOption(o);
        }
        return options;
    }

    public static CommandLine parseOptions(String[] args, ApacheCLIOptions[] clios) throws ParseException
    {

        Options options = allOptions(clios);

        //Create a parser
        CommandLineParser parser = new DefaultParser();

        CommandLine cml = null;
        //parse the options passed as command line arguments
        try
        {
            cml = parser.parse(options, args);
        }
        catch (MissingOptionException e)
        {
            //System.out.println("Problem while parsing options: " + e.getMessage());
            throw  e;
        }
        return cml;
    }



    public static boolean checkForHelp(String[] args) throws ParseException {
        return checkForHelp(args, "h", "help");
    }

    public static boolean checkForHelp(String[] args, String helpShortOpt, String helpLongOpt) throws ParseException {
        Option helpOption = Option.builder(helpShortOpt)
                .longOpt(helpLongOpt)
                .required(false)
                .hasArg(false)
                .build();
        boolean hasHelp = false;

        Options options = new Options();

        try
        {
            options.addOption(helpOption);
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args, true);
            if (cmd.hasOption(helpOption.getOpt()))
            {
                hasHelp = true;
            }

        }
        catch (ParseException e)
        {
            //System.out.println("Error parsing Command Line options: " + e.getMessage());
            throw e;
        }
        return hasHelp;
    }

    public static void printHelp(ApacheCLIOptions[] clios, String helpMsgSyntax, String helpMsgHeader, String helpMsgFooter)
    {
        Options options = allOptions(clios);
        HelpFormatter formatter = new HelpFormatter();

        formatter.printHelp(helpMsgSyntax,
                helpMsgHeader,
                options, helpMsgFooter);
    }
}
