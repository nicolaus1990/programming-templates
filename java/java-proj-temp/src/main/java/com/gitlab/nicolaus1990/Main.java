package com.gitlab.nicolaus1990;
/*
 * Main class
 *
 * Example cml args: -D -l C:\Users\nmoeller\Desktop\logFolder\projName  -a action-1
 */
public class Main
{
    public static void main(String[] args)
    {
        ProjName p = new ProjName();
        p.start(args);
    }
}
