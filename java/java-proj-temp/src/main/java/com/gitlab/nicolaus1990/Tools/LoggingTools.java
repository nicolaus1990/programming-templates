package com.gitlab.nicolaus1990.Tools;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/*
 * For logging either:
 *       i)  set logDirectory variable and use rolling file appender in root logger
 *         node in 'log4j2_OLD.xml', or...
 *     ii) create appender programmatically. (NOT IMPLEMENTED)
 */
public class LoggingTools {

    private static Logger logger;


    /*
     * SLF4J with log4j stuff
     */
    /*
     * Assumes that in log4j2_OLD.xml file has sys:logDirectory (like: ${sys:logDirectory}) and
     * sys:logLevel.
     * After invoking this method, you can instantiate the logger with:
     *      logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
     */
    public static void initLogging(String logDir, String logSuffix, boolean debugMode){
        System.setProperty("logDirectory", logDir);
        System.setProperty("logSuffix", logSuffix);
        if (debugMode){
            System.setProperty("logLevel", "DEBUG");
        } else {
            System.setProperty("logLevel", "INFO");
        }

        logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
        logger.debug("Logging initilized");
    }

    /*
     * Assumes that in log4j2_OLD.xml file has sys:logDirectory (like: ${sys:logDirectory}) somewhere.
     * If callingClass is null, it will set LoggingTools as the class.
     * DEPRECATED!
     */
    public static Logger setupLoggerOLD(String logDir, boolean debugMode, Class<?> callingClass){

        System.setProperty("logDirectory", logDir);//For rolling file appender
        Logger logger = null;
        if (callingClass == null){
            logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
        } else {
            logger = LoggerFactory.getLogger(callingClass);
        }
        org.apache.logging.log4j.core.LoggerContext ctx =
                (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
        ctx.reconfigure();

        Configuration config = ctx.getConfiguration();
        LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        if(debugMode){
            //LogManager.getRootLogger().setLevel(Level.DEBUG);
            //LogManager.get
            //org.apache.logging.log4j.core.config.Configurator.setLevel("", Level.DEBUG);
            loggerConfig.setLevel(Level.DEBUG);

        } else {
            loggerConfig.setLevel(Level.INFO);
        }
        ctx.updateLoggers();  // This causes all Loggers to refetch information from their LoggerConfig.
        return logger;
    }



    /*
     * Logging levels: ALL < DEBUG < INFO < WARN < ERROR < FATAL < OFF
     */
    public static void testLog() throws IOException {

        //Test logger:
        logger.debug("Debug log message");
        logger.info("Information log message");
        logger.error("Error log message");
        logger.warn("Warning log message");
    }
}
