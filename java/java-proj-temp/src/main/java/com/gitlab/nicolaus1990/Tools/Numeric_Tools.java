package com.gitlab.nicolaus1990.Tools;

public class Numeric_Tools {


    public static int integerToDigits(int number){
        //This has a lot of issues...
        return (number == 0) ? 1 : (int) Math.floor(Math.log10(number)) + 1;
    }

    //Credit: https://stackoverflow.com/questions/332079/in-java-how-do-i-convert-a-byte-array-to-a-string-of-hex-digits-while-keeping-l
    public static String bytes2Hex(byte[] bs){
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < bs.length; i++) {
            if ((0xff & bs[i]) < 0x10) {
                hexString.append("0"
                        + Integer.toHexString((0xFF & bs[i])));
            } else {
                hexString.append(Integer.toHexString(0xFF & bs[i]));
            }
        }
        return hexString.toString();
    }
}
