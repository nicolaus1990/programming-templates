package Tools;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class String_ToolsTest {


    @Test
    public void removeRegex() {

        String s1 = String_Tools.removeRegex("#### Hello there ####.ext","####(.*)####","%s");
        assertEquals("%s.ext", s1);

        String s2 = String_Tools.removeRegex("# Remove me #-# Remove me too #.ext","#.*#","%s");
        assertEquals("%s.ext", s2);

        String s3 = String_Tools.removeRegex("# Remove me #-# Remove me too #.ext","#[^#]*#","%s");
        assertEquals("%s-%s.ext", s3);
    }



    @Test
    public void regexStuff() {
        //Test end of string to be of some form
        String text    = "path/to/some/file.xml";

        String xml_ending_patternString = ".*\\.xml$";
        Pattern pattern = Pattern.compile(xml_ending_patternString);
        Matcher matcher = pattern.matcher(text);
        boolean matches = matcher.matches();

        assertTrue(matches);

        //String is of form: "alpha,beta,gama,...,whateva", specifically: not allowed: "alpha,,gamma,.." or ",alpha..."
        //String regex = "^[a-zA-Z0-9]$";   [^abc]
        String comma_sep_patternString = "^[^,]+(,[^,]+)*$";
        Pattern pattern2 = Pattern.compile(comma_sep_patternString);
        Matcher matcher2 = pattern2.matcher("alpha,beta,gama,whateva");
        assertTrue(matcher2.matches());
        Matcher matcher3 = pattern2.matcher("alpha,beta,gama,,whateva");
        assertFalse(matcher3.matches());

        //TODO: ArgumentParser missing: This will fail. (needs preferably another regex).
        Matcher matcher4 = pattern2.matcher("a,   ");
        //assertFalse(matcher4.matches());

    }
}