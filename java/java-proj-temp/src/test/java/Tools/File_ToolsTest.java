package Tools;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

import static org.junit.Assert.*;

public class File_ToolsTest {

    private static Logger logger;

    @BeforeClass
    public static void setUpOnce() {
        //logger = LoggingTools.setupLogger("C:\\Users\\nmoeller\\Desktop\\",true, MethodHandles.lookup().lookupClass());
        LoggingTools.initLogging("C:\\Users\\nmoeller\\Desktop\\", "TESTS" ,true);
        logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
        logger.info("Logging setup successfull!");
    }

    @Before
    public void setUp() {
        //Setup before each test method.
    }

    @Test
    public void replaceExtension() {

        String s1 = File_Tools.replaceExtension("/path/to/file","txt");
        assertEquals("/path/to/file.txt", s1);

        String s2 = File_Tools.replaceExtension("/path/to/file.extension","txt");
        assertEquals("/path/to/file.txt", s2);
    }
}