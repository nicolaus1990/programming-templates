#!/bin/bash
# Idea: Input: 
#    --home-path Absolute path to home 
#    --target Path where emacs config dir is located
# use getopt: https://sookocheff.com/post/bash/parsing-bash-script-arguments-with-shopts/
# But for now:
EMACS_INIT_FOLDER_PATH=./nikkis-emacs/
# ln --symbolic /home/nikki/.gitconfig $EMACS_INIT_FOLDER_PATH
# ln --symbolic /home/nikki/.org-timestamps $EMACS_INIT_FOLDER_PATH
ln --symbolic /home/nikki/.m2 $EMACS_INIT_FOLDER_PATH
ln --symbolic /home/nikki/.lein $EMACS_INIT_FOLDER_PATH
ln --symbolic /home/nikki/.gnupg $EMACS_INIT_FOLDER_PATH
ln --symbolic /home/nikki/.mozilla $EMACS_INIT_FOLDER_PATH
ln --symbolic /home/nikki/.bash_history $EMACS_INIT_FOLDER_PATH
