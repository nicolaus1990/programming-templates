@echo off

setlocal enableextensions enabledelayedexpansion

echo Setting paths
REM set HOME=C:\Users\nmoeller\Desktop\nikkis-emacs-inits\work-environment
set HOME=C:\Users\nmoeller\Documents\programming-templates\elisp\init-files\work-environment\
REM set HOME="C:\Users\nmoeller\Desktop\emacs-env\spacemacs\"
REM Set-Item -Path Env:HOME -Value %CONFIGPATH%
REM set EMACSPATH="C:\Users\nmoeller\Documents\Software installed\emacs-26.2-x86_64\bin"
set EMACSPATH=C:\Users\nmoeller\Documents\emacs\bin


echo Running emacs...
cd %EMACSPATH%
start "" .\runemacs.exe %*

REM What does this make?
REM         setlocal enableextensions enabledelayedexpansion
REM Comments start with REM
REM Paths with " do not work.
REM Set-Item works on powershell only (?) 
REM %* is like $@ in bash (all cli args) (?)