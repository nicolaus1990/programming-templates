(ns proj-templ-cljs.routing-test
  (:require [clojure.test :refer :all]
            [environ.core :refer [env]]
            [compojure.core :as cc]
            [compojure.route :as route]
            [proj-templ-cljs.routing.site-home-docs :as rshd]))


(defn example-test []
  (is (= 1 (- 2 1))
      "Test"))

(deftest some-test
  (example-test)
  "?")
