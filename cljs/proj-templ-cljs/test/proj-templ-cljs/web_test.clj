(ns proj-templ-cljs.web-test
  (:require [clojure.test :refer :all]
            [environ.core :refer [env]]
            [clojure.java.jdbc :as db]
            [proj-templ-cljs.web :refer :all]))


(deftest example-test
  (is (= 2 (+ 1 1)) "Addition is working?"))

(deftest check-log-level
  (is (= "debug" (env :log-level))
      "Check log-level retrieved from profile."))

;; (deftest database-url
;;   (is (= "postgres://localhost:5432/nikkis_server_test"
;;          (env :database-url))
;;       "Database url taken from profile?"))

(defn check-db-spec []
  (let [db-spec (env :db-spec)]
    (is (and  (= "org.postgresql.Driver" (:classname db-spec))
              (= "postgresql" (:subprotocol db-spec)))
        "Check db-spec and test profile working.")))

(defn connect2db []
  (is (db/get-connection (env :db-spec))
      "Test connection to db"))

(deftest profile-settings
  (check-db-spec)
  (connect2db)
  "Database settings correct?")
