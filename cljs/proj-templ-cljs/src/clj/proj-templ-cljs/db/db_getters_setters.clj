(ns proj-templ-cljs.db.db-getters-setters
  (:require [clojure.java.jdbc :as db]
            ;;Environment variables
            [environ.core :refer [env]]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log])
  ;(:gen-class)
  )


(defn db-spec []
  (env :db-spec
       ;;Or heroku:
       (env :database-url)))

;; CREATE TABLE my_users
;; (
;;  id SERIAL PRIMARY KEY,
;;  username VARCHAR(64) NOT NULL UNIQUE,
;;  password VARCHAR(64) NOT NULL
;; );
;; INSERT INTO my_users (username,password) VALUES ('', '');
;; Authdata should look like this: {:admin "secret"}, where 'admin' is the
;; username. (i.e. : keyword: username, value the password).
(defn get-authdata
  ([] (get-authdata (db-spec)))
  ([db-spec]
   (let [lseq (db/with-db-connection [conn db-spec]
                (db/query conn ["select username, password from my_users"]))]
     (reduce (fn [acc user] (assoc acc
                                   (keyword  (:username user))
                                   (:password user)))
             {}
             lseq))))


(defn get-sayings
  ([] (get-sayings (fn [s] s)))
  ([do-smth]
   (db/with-db-connection [conn (db-spec)]
     (for [s (db/query conn
                       ["select content from sayings"])]
       (do-smth s)))
   ;; Above is equal to:
   ;; (with-open [conn (db/get-connection db-spec)]
   ;;   (for [s (db/query (env :db-spec)
   ;;                     ["select content from sayings"])]
   ;;     (do-smth s)))
   ))

(defn set-saying [say]
  (db/with-db-connection [conn (db-spec)]
    (db/insert! conn "sayings" {"content" say})
    )
  )

