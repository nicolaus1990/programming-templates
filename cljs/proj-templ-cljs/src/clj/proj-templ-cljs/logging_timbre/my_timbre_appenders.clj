(ns proj-templ-cljs.logging-timbre.my-timbre-appenders
  "My rolling file appender. Inspired by:
  https://github.com/ptaoussanis/timbre/blob/master/src/taoensso/timbre/appenders/3rd_party/rolling.clj#L73"
  {:author "Nikki"}
  (:require ;[taoensso.timbre.appenders.core :as core-appenders]
   [clojure.java.io :as io])
  (:import  [java.text SimpleDateFormat]
            [java.util Calendar]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; My variables
;;; For rolling file-appender:
(def ^{:private true} log-path "./log/log-timbre-nikkis-log.txt")
(def ^{:private true} rolling-frequency :daily)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;; Normal console-appender


(defn console-appender-fn [data]
  (let [{:keys [instant level output_]} data
        output-str (force output_)]
    (println output-str)))

(def console-appender
  "Console-appender"
  {:enabled?   true  ; Please enable new appenders by default
   :async?     false ; Use agent for appender dispatch? Useful for slow dispatch
   :min-level  :debug   ; nil (no min level), or min logging level keyword
   :output-fn :inherit ; or a custom (fn [data]) -> string
   ;; The actual appender (fn [data]) -> possible side effects
   :fn console-appender-fn})


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Rolling file-appender fns
(defn- rename-old-create-new-log [log old-log]
  (.renameTo log old-log)
  (.createNewFile log))

(defn- shift-log-period [log path prev-cal]
  (let [postfix (-> "yyyyMMdd" SimpleDateFormat. (.format (.getTime prev-cal)))
        old-path (format "%s.%s" path postfix)
        old-log (io/file old-path)]
    (if (.exists old-log)
      (loop [index 0]
        (let [index-path (format "%s.%d" old-path index)
              index-log (io/file index-path)]
          (if (.exists index-log)
            (recur (+ index 1))
            (rename-old-create-new-log log index-log))))
      (rename-old-create-new-log log old-log))))

(defn- log-cal [date] (let [now (Calendar/getInstance)] (.setTime now date) now))

(defn- prev-period-end-cal [date pattern]
  (let [cal (log-cal date)
        offset (case pattern
                 :daily 1
                 :weekly (.get cal Calendar/DAY_OF_WEEK)
                 :monthly (.get cal Calendar/DAY_OF_MONTH)
                 0)]
    (.add cal Calendar/DAY_OF_MONTH (* -1 offset))
    (.set cal Calendar/HOUR_OF_DAY 23)
    (.set cal Calendar/MINUTE 59)
    (.set cal Calendar/SECOND 59)
    (.set cal Calendar/MILLISECOND 999)
    cal))

(defn- make-rolling-appender
  "Returns a Rolling file appender. Opts:
    :path    - logfile path.
    :pattern - frequency of rotation, e/o {:daily :weekly :monthly}."
  [& [{:keys [path pattern level]
       :or   {path    "./timbre-rolling.log"
              pattern :daily
              level :debug}}]]
  {:enabled?   true
   :async?     false
   :min-level  level ;nil
   :rate-limit nil
   :output-fn  :inherit
   :fn
   (fn [data]
     (let [{:keys [instant output_]} data
           output-str (force output_)
           prev-cal   (prev-period-end-cal instant pattern)]
       (when-let [log (io/file path)]
         (try
           (when-not (.exists log)
             (io/make-parents log))
           (if (.exists log)
             (if (<= (.lastModified log) (.getTimeInMillis prev-cal))
               (shift-log-period log path prev-cal))
             (.createNewFile log))
           (spit path (with-out-str (println output-str)) :append true)
           (catch java.io.IOException _)))))})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;; Timbre's log config map
(defn get-log-config
  ;;TODO: Better use map as arg and destructuring
  ([] (get-log-config :debug true :debug))
  ([level] (get-log-config level true level))
  ([level writeToFile] (get-log-config level writeToFile level))
  ([level writeToFile console-level]
   "Console: Only logs level info.
  Rolling-file-appender: Daily and all levels (level: debug)."
   {:level :debug ;;Don't change this level.
    ; :talk := console, :spit := writer
    ;Keys (:spit, :talk) in appenders don't matter (apparently).
    :appenders (let [appenders {:talk (merge console-appender 
                                           {:min-level console-level})}]
                 (if writeToFile
                   (merge appenders
                          {:spit (make-rolling-appender
                                  {:path log-path
                                   :level level
                                   :pattern rolling-frequency})})
                   appenders))
    ;; If merge is done, appenders is going to look like this:
    ;; {:spit (make-rolling-appender {:path log-path
    ;;                                :level level
    ;;                                :pattern rolling-frequency})
    ;;  :talk (merge console-appender {:min-level :info})}
    }))

