(ns proj-templ-cljs.routing.middleware.tools
  (:require [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.route :as route]
            [ring.util.response :refer [file-response]]
            [environ.core :refer [env]]
            [clojure.string :as s]
            [clojure.java.io :as io]
            [proj-templ-cljs.routing.middleware.tools :as mid]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Convert clean url in request to dirty url
;;; (for compojure.route.resource to handle)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn clean-url->dirty-url [url]
  ;; Clean urls don't have an extension (".html",...) nor "index.html".
  (let [index-of-slash (or (s/last-index-of url "/") -1)
        index-of-point (or (s/last-index-of url ".") -1)
        len (count url)]
    ;;If (last) slash before (last) point, then extension was given (url dirty).
    ;;Else, if it's a uri-subcategory (like a folder) return index.html file.
    ;;Else, it's supposed to be an html file (url was clean).
    (cond (< index-of-slash index-of-point)    url
          (= len (inc index-of-slash))         (str url "index.html")
          :else                                (str url ".html")
          )))


(defn add-slash-to-url [uri-sub url]
  ;; A slash to "/<uri-sub>" needs to be added, else 'clean-url->dirty-url'
  ;; will dirtify link to "<uri-sub>.html". (and we want "/<uri-sub>/index.html").
  (cond (= url (str "/" uri-sub)) (str url "/")
        (= url (str "/" uri-sub "/")) url
        :else url ;This shouldnt have happened
        ))


(defn clean-url->dirty-url-middleware [handler uri-sub]
  ;; Makes out of cleaned urls dirty ones. Adds a slash to url if it's the
  ;; base-uri (searching then for the 'index.html' file in base-uri
  ;; folder. Params: uri-sub shouldn't contain any slashes.
  (fn [req]
    (-> req
        (update :uri (partial add-slash-to-url uri-sub))
        (update :uri clean-url->dirty-url)
        (handler))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Wrappers 
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn- check-referer [req base-url]
  (if-let [referer (get-in req [:headers "referer"])]
    (let [len-base (.length base-url)
          len-ref (.length referer)]
      (if (<= len-base len-ref)
        (if (= base-url
               (.substring referer (- len-ref len-base)))
          true
          nil)
        nil))
    nil))


;;TODO: Implementation is buggy: If handler does not correspond to referer it
;;could possibly call handler if only the last characters of referer equal
;;base-url. (See 'check-referer').
;;TODO: Strange that ring/compojure don't provide this functionality... is
;;this implemented the wrong way?
(defn wrap-check-referer [handler base-url]
  ;; When browser fetches for resources (js,css,...) uri are relative. This
  ;; wrapper checks if the given handler is to be called with this request.
  ;; This middleware is to used after 'wrap-resources'.
  ;;Note: referer example: 'http://my-web-address.com/path/to/something'.
  (fn [req]
    ;; Call handler if uri is base-url or...
    (if (or (= (:uri req)  base-url)
            (check-referer req base-url))
      (handler req)
      ;;.. if referer of uri is base-url
      nil)))
