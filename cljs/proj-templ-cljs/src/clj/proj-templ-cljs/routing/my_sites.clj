(ns proj-templ-cljs.routing.my-sites
  (:require [clojure.string :as s]
            [clojure.java.io :as io]
            ;;Web-dev
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.route :as route]
            [ring.util.response :refer [file-response redirect]]
            [hiccup.core :as hi]
            [hiccup.form :as hif]
            [hiccup.element :as hie]
            ;;Sites
            [proj-templ-cljs.routing.site-feeds :as sf]
            ;;DB
            [proj-templ-cljs.db.db-getters-setters :as db]
            ;;Environment vars
            [environ.core :refer [env]]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log]))


;; (defn splash []
;;   {:status 200
;;    :headers {"Content-Type" "text/plain"}
;;    :body "Hello from Heroku"})

(def quick-form 
  (hi/html
   (hif/form-to {:enctype "multipart/form-data"}
    [:post "/things-out"]
   (hif/text-field "Store")
   (hif/submit-button {:class "btn" :name "submit"} "Save")
   (hif/submit-button {:class "btn" :name "submit"} "action2"))))



(defn splash []
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (concat
          ["<hr /><ul>"]
          quick-form
          (db/get-sayings #(format "<li>%s</li>" (:content %)))
          ["</ul>"]
          )})


(defroutes my-sites
  (GET "/things" []
       (splash))
  (POST "/things-out" [:as request]
        (let [s (request :multipart-params)]
          (log/debug (str "Multipart-params of form: " s))
          (case (get-in s ["submit"])
            "Save"
            (db/set-saying (get-in s ["Store"]))
            "action2"
            (log/debug "Not implemented yet.")
            ;Default:
            (log/error "Shouldn't have happened!"))
          (redirect "/things"))
        )
  (GET "/myFeeds" []
       (sf/feed-site)))



