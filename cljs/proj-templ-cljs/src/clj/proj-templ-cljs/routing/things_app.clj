(ns proj-templ-cljs.routing.things-app
  (:require [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.route :as route]
            [ring.util.response :refer [file-response]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.util.response :refer [redirect]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [hiccup.middleware :refer [wrap-base-url]]
            [environ.core :refer [env]]
            [clojure.string :as s]
            [clojure.java.io :as io]
            [proj-templ-cljs.routing.middleware.tools :as mid]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log]))


(def app-name "things2")
(def base-uri (str "/" app-name))
(def root-folder "public/app_things")


;;  NOTE: For development: App will not be minified, and thus links to source
;;  files in different javascript files will have links to other files
;;  relative to "resouces/public" folder (app location). Thus, links would
;;  look like, for example: "app_name/path/to/some/file.js" For this reason,
;;  this handler requires "resource/public" to be public. (Use in outer most
;;  route: (route/resources "/" {:root "public/"}).
(def things-handler
  (as-> (route/resources base-uri {:root root-folder}) $
    ;; Wrap-resource is for html resources (src/href attributes are relative
    ;; links to css/js files).
    ;(wrap-base-url $ base-uri)
    (wrap-resource $ root-folder)
    ;; Additional wrappers for wrap-resource (more information:
    ;; https://github.com/ring-clojure/ring/wiki/Static-Resources
    ;;(wrap-content-type $)
    ;;(wrap-not-modified $)
    ;; Clean urls
    (mid/clean-url->dirty-url-middleware $ app-name)
    ;;(GET (str base-uri "*") [] $)
    ;;TODO: Strange that ring/compojure don't provide this functionality... is
    ;;this implemented the wrong way?
    ;;TODO: In browser: need to reload the page to fetch correct resources.
    ;;Probably a browser thing? Not fetching if it has the same uri? Remember
    ;;the problem: if relative link (in request: :uri) to resources (css for
    ;;e.x.) in one app is the same as in the other one, ring/compojure needs
    ;;to know somehow which files to route to.
    (mid/wrap-check-referer $ base-uri)
    ;;(GET (str base-uri "*") [] $)
    ))
