(ns proj-templ-cljs.routing.site-home-docs
  (:require [clojure.java.io :as io]
            ;;Webdev
            [ring.util.response :refer [redirect]]
            [compojure.route :as route]
            [compojure.core :refer ;:all
                                   [GET]]
            ;;Authorization and authentication
            [proj-templ-cljs.auth.buddy-session-login :as b]
            [proj-templ-cljs.db.db-getters-setters :as db]
            ;;Environment variables
            [environ.core :refer [env]]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log]))


(def route-name "homedocs")
(def base-url (str "/" route-name))
(def root-folder (str  "private/" route-name "/"))


(defn first-link-fix-middleware [handler]
  ;; This middleware could be prevented: It's just because we're redirecting
  ;; with "/homedocs" in "login-authenticate".
  (fn [req] 
    (if (or  (= (:uri req) base-url)
             (= (:uri req) (str base-url "/")))
      ;;We have to redirect and not just update uri in request (because for some
      ;;unknown reason it returns the html page but without the 'homedocs'
      ;;context, i.e.: links of file point to '/filename.html' and not to
      ;;'/homedocs/filename.html', for example).
      ;(handler (update req :uri (fn [_]  "/homedocs/index.html")))
      (redirect (str base-url "/index.html"))
      (handler req))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Routes and Middlewares                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def route-home
  (let [route (route/resources base-url
                               {:root root-folder})]
    (as-> route $
      ;(wrap-resource $ root-folder)
      ;; Additional wrappers for wrap-resource (more information:
      ;; https://github.com/ring-clojure/ring/wiki/Static-Resources
      ;(wrap-content-type $)
      ;(wrap-not-modified $)
      (first-link-fix-middleware $)
      (b/auth-middleware $)
      ;;GET wrapper has to be at the end. 
      (GET (str base-url "*") [] $)
      )))


(defn get-handler []
  (b/secure-handler route-home (db/get-authdata))
  )
