(ns proj-templ-cljs.routing.api-todo
  (:require [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [environ.core :refer [env]]
            [clojure.string :as s]
            [clojure.java.jdbc :as db]
            [clojure.java.io :as io]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log]))


(defn record [input]
  (db/insert! (env :database-url "postgres://localhost:5432/kebabs")
              :sayings {:content input}))

(defroutes handlers
  (context "/api" []
           ;;/api/kebab?input=some_input_right_here
           (GET "/kebab" {{input :input} :params}
                ;;Do something with input
                ;(record input)
                {:status 200
                 :headers {"Content-Type" "text/plain"}
                 :body input}))
  )
