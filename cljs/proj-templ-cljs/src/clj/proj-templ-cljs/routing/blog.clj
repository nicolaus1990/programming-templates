(ns proj-templ-cljs.routing.blog
  (:require [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.route :as route]
            [ring.util.response :refer [file-response redirect resource-response]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [proj-templ-cljs.routing.middleware.tools :as mid]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log]))

;;Requires: environ var: ":path-blog"

(def base-uri "/thoughts")
(def root-folder "public/blog_nikkis_thoughts")


(def my-blog-handler 
  (let [route (route/resources base-uri
                               {:root root-folder})]
    (as-> route $
      ;;(wrap-subdirectories $)
      (wrap-resource $ root-folder)
      ;; Additional wrappers for wrap-resource (more information:
      ;; https://github.com/ring-clojure/ring/wiki/Static-Resources
      (wrap-content-type $)
      (wrap-not-modified $)
      ;; Cleaning url middleware has to come before any redirects or responses.
      (mid/clean-url->dirty-url-middleware $ "thoughts")
      ;;GET wrapper has to be at the end. 
      ;(GET (str base-uri "*") [] $)
      )))

(defroutes my-blog-handler-and-as-root-page
  (GET "/" [] (redirect base-uri))
  my-blog-handler)


;;;; NOT BEING USED
;;;; Taken from Cryogen code
;; (defn path
;;   "Creates path from given parts, ignore empty elements"
;;   [& path-parts]
;;   (->> path-parts
;;        (remove s/blank?)
;;        (s/join "/")
;;        (#(s/replace % #"/+" "/"))))
;; (defn wrap-subdirectories
;;   [handler]
;;   (fn [request]
;;     (let [req-uri (.substring (url-decode (:uri request)) 1)
;;           res-path (if (or (= req-uri "") (= req-uri "/"))
;;                      (path "/index.html")
;;                      (path (str req-uri ".html")))]
;;       (or (resource-response res-path {:root root-folder})
;;           (handler request)))))
