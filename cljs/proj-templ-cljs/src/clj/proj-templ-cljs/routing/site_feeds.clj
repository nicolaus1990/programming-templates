(ns proj-templ-cljs.routing.site-feeds
  (:require [clojure.xml :as cx]
            [clojure.java.jdbc :as db]
            [clojure.string :as s]
            [clojure.java.io :as io]
            ;;Envrionment vars
            [environ.core :refer [env]]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log]
))

;;cx/parse: given a url (xml feed), returns map representing xml.

(defn serialize [m sep]
  (str (clojure.string/join sep
                            (map (fn [[_ v]] v) m)) "\n"))


(defn feed-site-working []
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (concat
          ["<hr /><ul><li>"]
          (cx/parse "https://feeds.megaphone.fm/futureperfect" )
          ["</li></ul>"]
          )})

(defn get-feed-html [feed]
  (loop [xml (cx/parse feed)
         ;formatStr "<ul><li>%s</li></ul>"
         ans []]
    (if (empty? xml)
      ans
      (recur (:content xml);TODO
             (str "<li><ul></ul></li>")))))

;; (for [feed ]
;;   (concat ["<li>" feed]
;;           ["<ul>"]
;;           (format "<li>%s</li>"
;;                                         ;(serialize (cx/parse feed) ";\n")
;;                   (cx/parse feed)
;;                   )
;;           ["/ul"]
;;           ["</li>"]
;;           )
;;   )
;; ["</ul>"]
  

(defn get-feeds-html []
  (loop [feeds (list "https://feeds.megaphone.fm/futureperfect")
         ans []]
    (if (empty? feeds)
      ans
      (recur (rest feeds) (into ans (get-feed-html (first feeds)))))))

;;concat format
;;list str serialize cx/parse
;; Don't use concat: https://stuartsierra.com/2015/04/26/clojure-donts-concat
(defn feed-site []
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (get-feeds-html)
          })
