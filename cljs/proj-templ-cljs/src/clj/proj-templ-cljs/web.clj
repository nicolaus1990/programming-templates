(ns proj-templ-cljs.web
  (:require [clojure.string :as s]
            [clojure.java.io :as io]
            [clojure.pprint :as pprint]
            ;;Web-dev
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.handler :as ch]
            [compojure.route :as route]
            ;[ring.middleware.defaults :as rmid] ;;ring-defaults NOT WORKING
            [ring.adapter.jetty :as jetty]
            [ring.util.response :refer [file-response]]
            ;;My routes
            [proj-templ-cljs.routing.my-sites :as ms]
            [proj-templ-cljs.routing.blog :as blog]
            [proj-templ-cljs.routing.todo-app :as todo]
            [proj-templ-cljs.routing.things-app :as things]
            [proj-templ-cljs.routing.api-todo :as api-todo]
            [proj-templ-cljs.routing.site-home-docs :as priv-home-docs]
            ;;Environment variables
            [environ.core :refer [env]]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log])
  (:gen-class))


(defroutes not-found-handler
  (ANY "*" []
       (route/not-found (slurp (io/resource "404.html")))))


;(defn error-handler [req] ("oops"))

(defn pformat [& args]
  ;;Pretty print but *out* is Stringbuffer and not stdout.
  (with-out-str
    (apply pprint/pprint args)))

(defn log-middleware [handler]
  ;;Middleware to debug request
  (fn [request]
    (let [response (handler request)]
      (log/debug (str "RECEIVED: " (pformat request) ))
      (log/debug (str "ANSWERED: " (pformat response)))
      response)))


(defn build-handlers []
  ;;Join all handlers together
  (routes ;;My static sites: blog,...
          ;;;;blog/my-blog-handler
          blog/my-blog-handler-and-as-root-page
          ;;TODO: ring-defaults not working
          ;; Ring-defaults. There are 4 configs available for ring-defaults:
          ;;    api-defaults, site-defaults, secure-api-defaults, secure-site-defaults
          ;; (rmid/wrap-defaults blog/my-blog-handler-and-as-root-page
          ;;                     rmid/site-defaults)
          (priv-home-docs/get-handler)
          ;;Apps
          ms/my-sites
          todo/todo-handler
          things/things-handler
          ;;Apis
          api-todo/handlers
          ;;Public resources
          (route/resources "/" {:root "public/"})
          ;;Anything
          not-found-handler
          ))

(defn make-app []
  (-> (build-handlers)
      ;;All general middlewares:
      log-middleware
      ;error-handler
      ;Development: Print stacktrace in browser
      ;(wrap-stacktrace)
      ))




(defn -main [& [port]]
  ;;Heroku will have a profile with an environ :port variable
  ;;Dev: port 5000
  (let [port (Integer. (or port (env :port) 5000))]
    (do
      (log/set-config! (l/make-config (env :log-level :debug)
                                      (env :log-write-to-file true)
                                      ;:info ;3rd param: console-level
                                      ))
      (log/info (str "Starting server at port " port))
      ;;TODO ch/site deprecated. Use ring-defaults
      (jetty/run-jetty (ch/site
                        (make-app)) ;; Before: #'app 
                       {:port port :join? false}))
    ))


;; For tests
(def test-app-for-figwheel
  ;;NOT WORKING (use default figwheel internal handler instead)
  ;; TODO: ring handler does not work well with figwheel because resources
  ;; aren't being resend when changed (some form of caching is enabled or some
  ;; false notification to browser that files haven't changed).
  (-> (routes todo/todo-handler
              things/things-handler
              ;;Public resources
              (route/resources "/" {:root "public/"}))
      log-middleware))
