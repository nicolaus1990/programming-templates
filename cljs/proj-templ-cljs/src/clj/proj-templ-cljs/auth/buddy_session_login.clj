(ns proj-templ-cljs.auth.buddy-session-login
  (:require [compojure.route :as route]
            [compojure.core :refer ;:all
                                   [GET POST routes]]
            [compojure.response :refer [render]]
            [clojure.java.io :as io]
            [ring.util.response :refer [response redirect]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.adapter.jetty :as jetty]
            ;;HTML templating
            [hiccup.core :as hi]
            [hiccup.page :as hip]
            [hiccup.element :as hie]
            ;;Authorization and authentication
            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            ;;Environment variables
            [environ.core :refer [env]]
            ;;Logging:
            [nikkis-timbre.mylog :as l][taoensso.timbre :as log])
  ;(:gen-class)
  )

;;;;;;;;;;;;;;; URL
(def path-url-login "/login-buddy")
(def path-url-logout "/logout-buddy")

;;;;;;;;;;;;;;; Helper fns
(defn rand-str [len] ;;For creating salt
  (apply str (take len (repeatedly #(char (+ (rand 26) 65))))))

(def js-client-script
  (str
   "
// Taken from: https://github.com/emn178/js-sha256
//sha256('Message to hash');
//var hash = sha256.create();
//hash.update('Message to hash');
//hash.hex();
function makeHash(message, salt) {
  sha256('Message to hash');
  var hash = sha256.create();
  var u = document.getElementById('user').innerHTML;
  //var p = document.getElementById('password').innerHTML;
  hash.update(u);
  document.getElementById('output').innerHTML = hash.hex();
  return hash.hex();
}
"))

   

;;;;;;;;;;;;;;; Login & Logout pages

(def login-page
  (hi/html
   (hip/xhtml-tag
    "en"
    [:head
     [:title "Login"]
     [:style {:type "text/css"}
      "body{background:#222;color:#ddd;margin:1em}"
      "h1{border:1px solid;padding:1em}"
      "td{padding:0.3em;border:1px solid #666}"]
     (hip/include-js
      "https://cdnjs.cloudflare.com/ajax/libs/js-sha256/0.9.0/sha256.js")
     (hie/javascript-tag js-client-script)]
    [:body [:h1 "Hello beautiful!"] [:hr]
     [:div 
      [:form {:method "post"}
       [:input {:type "text" :placeholder "User: " :name "username"}]
       [:input {:type "password" :placeholder "Password: " :name "password"}]
       [:input {:type "submit" :value "Submit"}]]
      ]
     [:div#user {:onclick "makeHash(1,2)"} "someval"]
     [:div#output ]])))

(def logout-page
  (hi/html
   [:div
    [:h1 "Come back again handsome!"]
    [:p "You just logged out."]]))

(def error-403
  (hi/html
   [:div
    [:h1 "Sorry, dude or gal. This is not allowed."]
    [:p "... please, stop!"]]))

(defn login
  ;; Login page controller
  ;; It returns a login page on get requests.
  [request]
  (render login-page request))

;; Logout handler
;; Responsible for clearing the session.

(defn logout
  [request]
  (-> (redirect path-url-login)
      (assoc :session {})))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Authentication                                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   ;; Authentication Handler
   ;; Used to respond to POST requests to /login.



(defn get-login-authenticate
  "Check request username and password against authdata
  username and passwords.

  On successful authentication, set appropriate user
  into the session and redirect to the value of
  (:next (:query-params request)). On failed
  authentication, renders the login page."
  [authdata]
  (fn [request]
    (let [username (get-in request [:form-params "username"])
          password (get-in request [:form-params "password"])
          session (:session request)
          found-password
          (let [data (get authdata (keyword username))]
            (do (log/debug data)
                data))
          ]
      (if (and found-password (= found-password password))
        (let [;; If no next-url found redirect to home file.
              next-url (get-in request
                               [:query-params  "next" ;:next
                                ;; Parameter 'next' is not internalized as keyword
                                ] "/"
                               )
              updated-session (assoc session :identity (keyword username))]
          (log/debug (str "Request looks like this: " request))
          (log/debug (str "Redirecting to next-url: " next-url))
          (-> (redirect next-url);without context (url is absolute)
              (assoc :session updated-session)))
        (login request)))))

(defn auth-middleware [handler]
  (fn [req]
    (if-not (authenticated? req)
      (throw-unauthorized)
      (handler req))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Routes and Middlewares                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; User defined application routes using compojure
;; routing library.
;; Note: We do not use middleware for authorization,
;; all of the authorization system is decoupled from
;; main routes.

(defn get-login-routes [authdata]
  (let [login-authenticate (get-login-authenticate authdata)]
    (routes
     (GET  path-url-login  [] login)
     (POST path-url-login  [] login-authenticate)
     (GET  path-url-logout [] logout)
     ;;and convenient logout:
     (GET "/logout" [] logout))))


;; User defined unauthorized handler
;; This function is responsible for handling unauthorized requests (when
;; unauthorized exception is raised by some handler).
;; See: 'throw-unauthorized'

(defn unauthorized-handler
  [request metadata]
  (cond
    ;; If request is authenticated, raise 403 instead
    ;; of 401 (because user is authenticated but permission
    ;; denied is raised).
    (authenticated? request)
    (-> (render error-403 request)
        (assoc :status 403))
    ;; In other cases, redirect the user to login page.
    :else
    (let [current-url (:uri request)]
      (redirect (format (str path-url-login "?next=%s") current-url)))))

;; Create an instance of auth backend.

(def auth-backend
  (session-backend {:unauthorized-handler unauthorized-handler}))


(defn secure-handler [route-to-secure auth-data]
  ;Authdata should look like this: {:admin "secret"}, where 'admin' is username.
  (as-> route-to-secure $
    (routes $ (get-login-routes auth-data))
    (wrap-authorization $ auth-backend)
    (wrap-authentication $ auth-backend)
    (wrap-params $)
    (wrap-session $)))
