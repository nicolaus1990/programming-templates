(defproject proj-templ-cljs "1.0.0-SNAPSHOT"
  :description "INSERT_DESCRIPTION_OF_PROJECT "
  :url "INSERT_URL"
  :license {:name "Eclipse Public License v1.0"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 ;[org.clojure/java.jdbc "0.7.8"]
                 ;;[org.clojure/tools.logging "0.3.1"]
                 ;[org.clojure/tools.cli "0.4.1"]
                 ;; DB related
                 ;; NOTE: heroku used this postgres:
                 ;;[org.postgresql/postgresql "9.4-1201-jdbc4"]"42.2.5.jre7"
                 ;[org.postgresql/postgresql "42.2.5.jre7"]
                 ;;Logging
                 [com.taoensso/timbre "4.7.0"]
                 ;; Web dev
                 [hiccup "1.0.5"]
                 [compojure "1.5.1"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 ;; Web dev frontend (cljs)
                 [org.clojure/clojurescript "1.10.339"]
                 ;;[kee-frame "0.3.1"]
                 ;;[re-frame "0.10.6" :exclusions [reagent]]
                 [reagent "0.8.1"]
                 ;; For security:
                 [buddy/buddy-auth "2.1.0"]
                 ;;[buddy/buddy-sign "2.2.0"]
                 ;;Environment variables
                 [environ "1.0.0"]]
  :main ^:skip-aot proj-templ-cljs.web
  :min-lein-version "2.0.0"
  :source-paths ["src/clj"]             ;source-path default: "src/".
  :plugins [[environ/environ.lein "0.3.1"]
            [lein-environ "1.1.0"]
            ;;For development
            [lein-ring "0.12.1"]]
  :hooks [environ.leiningen.hooks]
  :uberjar-name "proj-templ-cljs-standalone.jar"
  ;;Clojurescript
  :figwheel {:css-dirs ["resources/public/app_todo/css"
                        "resources/public/app_things/css"]
             ;;Don't use ring-handler: app route has middleware (or just
             ;;configuration?) for caching files enabled.
             ;;:ring-handler proj-templ-cljs.web/test-app-for-figwheel
             :server-port 5001 ;;default: 3449
             }
  ;;When invoking lein clean, remove generated javascript and target folder
  ;;Beware of paths that are removed; lein's protection overridden (see
  ;;metadata) to be able to remove something from "resources/public" folder.
  :clean-targets ^{:protect false} ["resources/public/app_todo/js/compiled/"
                                    "resources/public/app_things/js/compiled/"
                                    ;;Will remove also target-folder
                                    :target-path]
  :cljsbuild { ;;TODO cljsbuild config is deprecated! 
              :builds
              [{ ;;Production cljs build for todo-app
                :id           "min"
                :source-paths ["src/cljs/app_todo"]
                :compiler     {:output-to "resources/public/app_todo/js/compiled/app.js"
                               :optimizations :advanced
                               :parallel-build true}}
               { ;;Production cljs build for things-app
                :id           "min2"
                :source-paths ["src/cljs/app_things"]
                :compiler     {:output-to "resources/public/app_things/js/compiled/app.js"
                               ;;figwheel (and cljsbuild probably too) needs
                               ;;all output-dir folders in all builds to be
                               ;;unique (if none is set a default one,
                               ;;"resources/public/js/out", is going to be
                               ;;given). This is even if :parallel-build is
                               ;;set to false.
                               :output-dir "resources/public/app_things/js/temp/out"
                               :optimizations :advanced
                               :parallel-build true}}
               { ;;Dev cljs build for todo-app
                :id "todo"
                ;; The presence of a :figwheel configuration here will cause
                ;; figwheel to inject the figwheel client into build
                :figwheel true
                :source-paths ["src/cljs/app_todo"]
                :compiler {:main todo-app.core
                           :output-to
                           "resources/public/app_todo/js/compiled/app.js"
                           :output-dir
                           "resources/public/app_todo/js/compiled/out"
                           ;;asset-path = :output-dir minus your webroot directory
                           ;;    where webroot dir = "resources/public"
                           :asset-path "/app_todo/js/compiled/out"
                           :source-map-timestamp true
                           ;;:preloads [devtools.preload day8.re-frame-10x.preload]
                           ;;:parallel-build true
                           ;; :closure-defines {"re_frame.trace.trace_enabled_QMARK_"
                           ;;                   true
                           ;;                   kee-frame-sample.core/debug
                           ;;                   true}
                           ;; :external-config {:devtools/config
                           ;;                   {:features-to-install :all}
                           }}
               { ;;Dev cljs build for things-app
                :id "things"
                :figwheel true
                :source-paths ["src/cljs/app_things"]
                :compiler {:main things-app.core
                           :output-to
                           "resources/public/app_things/js/compiled/app.js"
                           :output-dir
                           "resources/public/app_things/js/compiled/out"
                           :asset-path "/app_things/js/compiled/out"
                           :source-map-timestamp true
                           }}
               ]}
  ;;For development
  :ring {:handler proj-templ-cljs.web/app}
  :profiles {;;:user {:env {:path-blog "public/blog/"}}
             :production {:env {:production true
                                }}
             :dev { ;;TODO: profiles can be inherited.
                   :dependencies [[ring/ring-devel "1.6.3"]]
                   :plugins [ ;;For cljs dev
                             [lein-figwheel "0.5.18"]
                             ;;Task cljsbuild will be available
                             [lein-cljsbuild "1.1.7"
                              :exclusions [[org.clojure/clojure]]]
                             ;;For deployment
                             [lein-heroku "0.5.3"]]
                   :env
                   {:log-level :debug
                    :log-write-to-file true
                    :path-html-login "public/login/login.html"
                    :db-spec {:classname "org.postgresql.Driver"
                              :subprotocol "postgresql"
                              :subname "//localhost:5432/database_dev_test"
                              :user "INSERT_USER_NAME"
                              :password "secret"
                              }
                    ;;:database-url "jdbc:postgres://localhost:5432/database_dev_test?user=INSERT_USER_NAME&password=secret"
                    }}
             :uberjar {:hooks       []
                       :omit-source true
                       :aot :all
                       ;;For clojurescript compile cljsbuild. TODO: Working?
                       :prep-tasks  ["compile" ["cljsbuild" "once" "min" "min2"]]}}
  :aliases { ;;;;Development
            ;;Cleans directory from generated files.
            "c" ["clean"]
            ;; Server
            "testServer" [;;"do" "cljsbuild" "once" "todo" "things,"
                          ;;"do" "cljsbuild" "once" "min" "min2,"
                          "with-profile" "+dev" "run"]
            "testWithRing" ["with-profile" "+dev" "ring" "server"]
            "startServer" ["run" "-p" "5000" "startServer"]
            ;;Apps (cljs) Test app with figwheel's hot-reloading css, (cl)js
            ;;files. Browse to: http://localhost:[PORT]/app_[APP-NAME]/index.html
            "fw" ["figwheel" "todo" "things"]
            ;;build the ClojureScript once (args after 'once' are ids of apps)
            "js" ["cljsbuild" "once" "todo" "things"]
            ;;watch src for changes and automatically rebuild them
            "jsa" ["cljsbuild" "auto" "todo" "things"]
            ;;Tests and help
            "ayuda" ["run" "-h"]
            ;;Note: Default "lein test" overriden with "lein with-profile +dev test"
            ;;For program to retrieve envrion vars from profile
            ;;it needs: lein-environ "1.1.0"
            "test" ["with-profile" "+dev" "test"]
            ;;Production
            ;;Uses plugin (Comma is necessary: first make uberjar then deploy).
            ;;Note: cljs's js files need to be compiled (See own uberjar map).
            "deploy" ["do" "uberjar," "heroku" "deploy"]
            })
