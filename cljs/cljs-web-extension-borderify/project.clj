(defproject cljs-web-extension-borderify "0.1"
  :description ""
  :url ""
  :license {:name "GNU General Public License"
            :url "https://www.gnu.org/licenses/gpl.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.946"]
                 ;; [org.clojure/core.async "0.3.465"]
                 ;; [org.clojure/core.match "0.3.0-alpha5"]
                 ;; [cljs-ajax "0.7.3"]
                 ]
  :plugins [[lein-cljsbuild "1.1.7"]]
  :profiles {:cljs-shared
             {:cljsbuild
              {:builds
               {;; :background
                ;; {:source-paths ["src/background" "src/lib"]
                ;;  :compiler {:closure-output-charset "us-ascii"
                ;;             :main cljs-web-extension-borderify.background}}
                :content
                {:source-paths ["src/content" ;; "src/lib"
                                ]
                 :compiler {:closure-output-charset "us-ascii"
                            :main cljs-web-extension-borderify.content}}}}}
             :dev [:cljs-shared
                   {:cljsbuild
                    {:builds
                     {;; :background
                      ;; {:figwheel true
                      ;;  :compiler {:optimizations :none
                      ;;             :output-to "ext/js/generated/background.js"
                      ;;             :output-dir "ext/js/generated/out-background"
                      ;;             :asset-path "js/generated/out-background"
                      ;;             :pretty-print true
                      ;;             :source-map true}}
                      :content
                      {:compiler {:optimizations :none;:whitespace
                                  :output-to "ext/js/generated/content.js"
                                  :output-dir "ext/js/generated/out-content"
                                  :asset-path "js/generated/out-content"
                                  :pretty-print true
                                  :source-map true;"ext/js/generated/content.js.map"
                                  }}}}
                    :dependencies [[com.cemerick/piggieback "0.2.2"]
                                   [org.clojure/tools.nrepl "0.2.10"]
                                   [figwheel-sidecar "0.5.14"]]
                    :plugins [[lein-figwheel "0.5.14"]]
                    :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}]
             :prod [:cljs-shared
                    {:cljsbuild
                     {:builds
                      {;; :background
                       ;; {:compiler {:optimizations :simple
                       ;;             :output-to "ext/js/generated/background.js"
                       ;;             :output-dir "ext/js/generated/out-background"
                       ;;             :pretty-print false
                       ;;             :source-map false}}
                       :content
                       {:compiler {:optimizations :simple
                                   :output-to "ext/js/generated/content.js"
                                   :output-dir "ext/js/generated/out-content"
                                   :pretty-print false
                                   :source-map false}}}}}]})
