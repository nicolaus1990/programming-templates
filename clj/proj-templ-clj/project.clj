(defproject proj-templ-clj "1.0.0-SNAPSHOT"
  :description "<INSERT_DESCRIPTION>"
  :url "<INSERT_URL>"
  :license {:name "Eclipse Public License v1.0"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 ;;Json
                 ;[metosin/jsonista "0.2.2"]
                 ;; Db
                 ;[org.xerial/sqlite-jdbc "3.23.1"]
                 ;[org.postgresql/postgresql "9.4-1201-jdbc4"]
                 ;[org.clojure/java.jdbc "0.7.8"]
                 ;;Command-line
                 [org.clojure/tools.cli "0.4.1"]
                 ;;Environment variables
                 [environ "1.0.0"]
                 ;;Logging
                 [com.taoensso/timbre "4.7.0"]]
  :main ^:skip-aot proj-templ-clj.main
  :min-lein-version "2.0.0"
  ;To use something as global plugin, merge in "$HOME/.lein/profiles.clj" for
  ;example: {:user {:plugins [[jonase/eastwood "0.3.5"]] }}
  :plugins [;;Environment variables
            [environ/environ.lein "0.3.1"]
            ;;Linter
            [jonase/eastwood "0.3.5"]
            ]
  :hooks [environ.leiningen.hooks]
  :uberjar-name "proj-templ-clj-standalone.jar"
  :profiles {:production {:env {:production true}}
             :dev {:env {:log-level :debug
                         :log-write-to-file true}
                   ;:dependencies [[org.xerial/sqlite-jdbc "3.23.1"]]
                   }
             }
  :aliases {"test" ["with-profile" "+dev" "test"]
            "ayuda" ["run" "-h"]
            ;Linters. To use another linter: joker --lint src/.../file.clj. Which
            ;should work with emacs (flycheck-joker).
            "linter" ["eastwood"]
            "example" ["run" "-f" "./somefile/file.txt" "-d" "someDatabaseUrl" "action1"]
            })

