(ns proj-templ-clj.logging-timbre.mylog
  "My log lib: Timbre-wrapper. (Sets config statically)."
  {:author "Nikki"}
  (:require [proj-templ-clj.logging-timbre.my-timbre-appenders
             :refer [get-log-config]]
            [taoensso.timbre :as timbre]))

;; Default: Log-level: debug and don't write to file
;;Logging levels hierarchy: debug < info < warn <error
(def log-config (get-log-config :debug false))

(defn make-config
  ;;TODO: make param a map and use destructuring
  ([log-level log-write-to-file]
   (get-log-config log-level log-write-to-file log-level))
  ([log-level log-write-to-file console-level]
   (get-log-config log-level log-write-to-file console-level)))


;;; Instead of writing own logging function (that wraps timbre/log anyway),
;;; user statically sets config variable and use timbre directly.
;;; If config wasn't set then it's log-config.
(timbre/set-config! log-config)


;;For tests:
(defn -main [& args]
  ;;NOTE: Logging levels hierarchy: debug < info < warn <error
  (do (timbre/info "Hello info ")
      (timbre/debug :debug "Debug..")
      (try 
        (throw (AssertionError. "Some assertion error or so....."))
        (catch AssertionError e
          (timbre/error "Hey! Some error caught successfully." e)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; My usage of timbre logging as local library (with lein chekout folder):
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment 

  ;;For example: Project file would look something like this:
  (defproject my-proj-name "1.0.0-SNAPSHOT"
    :dependencies [[environ "1.0.0"];Environment variables
                   ;; In checkout folder there should be a symbolic link to 
                   ;; library folders (nikkis-timbre for example). Every time 
                   ;; my-proj is run, libraries are "lein install"'ed. 
                   ;; NOTE: Version numbers have to be the same, and if conflicts
                   ;; check out ~/.m2 folder.
                   [nikkis-timbre "1.0.0"]
                   [com.taoensso/timbre "4.7.0"];Logging
                   [org.clojure/clojure "1.8.0"]]
    :min-lein-version "2.0.0"
    :plugins [[environ/environ.lein "0.3.1"]
              [lein-environ "1.1.0"]]
                                        ;:hooks [environ.leiningen.hooks];Need this?
    :profiles {:dev {:env
                     {:log-level :debug
                      :log-write-to-file false}}}
    :aliases {;Note: Default "lein test" overriden with "lein with-profile +dev test"
                                        ;For program to retrieve envrion vars from profile
                                        ;it needs: lein-environ "1.1.0"
              "test" ["with-profile" "+dev" "test"]})

  ;;And actual usage would look like this:
  (ns some-namespace
    ;;nikkis-timbre is in checkout folder
    (:require [nikkis-timbre.mylog :as mylogconfig]
              [environ.core :refer [env]]
              [taoensso.timbre :as log]))

  (defn some-fn [& args]
    (do (log/set-config! (l/make-config (env :log-level :debug)
                                        (env :log-write-to-file true)))
        (log/info "hello there")))

  ;;Requirements as one-liners for quick copy/paste:
  ;;[nikkis-server.nikkis-timbre.mylog :as nlog][taoensso.timbre :as log]

  )

  
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; My usage of timbre logging as submodule :
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment
  ;;For example: Project file would look something like this:
  (defproject my-proj-name "1.0.0-SNAPSHOT"
  :dependencies [[environ "1.0.0"];Environment variables
                 [com.taoensso/timbre "4.7.0"];Logging
                 [org.clojure/clojure "1.8.0"]]
  :min-lein-version "2.0.0"
  :plugins [[environ/environ.lein "0.3.1"]
            [lein-environ "1.1.0"]]
  ;:hooks [environ.leiningen.hooks];Need this?
  :profiles {:dev {:env
                   {:log-level :debug
                    :log-write-to-file false}}}
  :aliases {;Note: Default "lein test" overriden with "lein with-profile +dev test"
            ;For program to retrieve envrion vars from profile
            ;it needs: lein-environ "1.1.0"
            "test" ["with-profile" "+dev" "test"]})

  ;;And actual usage would look like this:
  (ns some-namespace
    (:require [environ.core :refer [env]]
              [my-proj-name.nikkis-timbre.mylog :as l];nikkis-timbre is folder name
              [taoensso.timbre :as log]))

  (defn some-fn [& args]
    (do (log/set-config! (l/make-config (env :log-level :debug)
                                        (env :log-write-to-file true)))
        (log/info "hello there")))

  )

