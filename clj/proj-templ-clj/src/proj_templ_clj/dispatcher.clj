(ns proj-templ-clj.dispatcher
  (:require [proj-templ-clj.tools.db :as tdb]
            [proj-templ-clj.tools.file :as tf]
            [clojure.string :as string]
            ;;Logging:
            [taoensso.timbre :as log]))


;;TODO: my-tasks is unnecessary...
;;Better: TODO: Make my-tasks some sort of datastructure with objs (change only
;;in one place).
(def my-tasks
  #{"action1" "action2" })

(def action-description
  ;; Description of action. Used for help message.
  (->> ["Actions:"
        "  action1             Description of action1."
        "                      Blablabla"
        "  sql2org             yadayadayada"]
       (string/join \newline)))

(defn valid-options? [action opts]
  ;; Checks if optional arguments are valid to execute action.
  (or ;For importing bookmarks you need to give a json file and db.
      (and (= action "action1")
           (:file opts) (:database opts))
      (and (= action "action2")
           (:file opts) (:path opts))))

(defn valid-input [arguments options]
  ;There can be only one argument (the action).
  (and (= 1 (count arguments))
       (my-tasks (first arguments))
       (valid-options? (first arguments) options)))

(defn choose-task [action options]
  (case action
    "action1"
    (log/info (str "Action1 with args:"
                   (:file options) (:database options) (:log options)))
    "action2"
    (log/info (str "Action2 with args:"
                   (:file options) (:database options) (:string options)))
    ;Else:
    (log/error (str "No task '" action "' is implemented."))
    ))
