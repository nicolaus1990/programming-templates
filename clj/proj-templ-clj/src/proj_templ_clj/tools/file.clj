(ns proj-templ-clj.tools.file
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;;;;;;;;;;;;;;;;;;;;
;;; Java File methods
;;;;;;;;;;;;;;;;;;;
(defn hidden? [file]
  ;Requires: file to be java.io/file
  (.isHidden file))

;; isSymbolicLink(java.io.File file) is the same as: 
;; if (file == null)
;;   throw new NullPointerException("File must not be null");
;;   File canon;
;;   if (file.getParent() == null) {
;;     canon = file;
;;   } else {
;;     File canonDir = file.getParentFile().getCanonicalFile();
;;     canon = new File(canonDir, file.getName());
;;   }
;;   return !canon.getCanonicalFile().equals(canon.getAbsoluteFile());
;; }
;; Credit: https://stackoverflow.com/questions/813710/java-1-6-determine-symbolic-links
(defn isSymbolicLink? [file]
  ;Requires: file needs to be a java.io.file
  (let [canon (if (nil? (.getParent file))
                file
                (io/file (.getCanonicalFile (.getParentFile file))
                         (.getName file)))]
    (not (.equals (.getCanonicalFile canon)
                  (.getAbsoluteFile canon)))))

;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; seq2file functions
;;; Example:
;;; (seq2file "~/Desktop/myFile.txt"
;;;           "My header"
;;;          ["a\nb" "c"])
;;; --> Will write to myFile: "My header <DATE>\na\nbc"
(defn seq2file
  ;; Writes sequence of 'objs'  to file (appends).
  ;; Every elem of seq is written.
  ([file seq header]
   (with-open [w (io/writer  file :append true)]
     (.write w (str header
                    (new java.util.Date) "\n"))
     (doseq [elem seq]
       (.write w elem)
       ))))
