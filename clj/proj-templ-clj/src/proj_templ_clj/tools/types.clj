(ns proj-templ-clj.tools.types
  (:require [clojure.string :as str]))

(defn map->str
  ;; {:id 1 :name "Bob" ...} --> "# :id:1 # :name:Bob\n"
  ;; delim: key/val delimiter, joinVals: in which way to join the key/val pairs.
  ;; prefix: A string to append before each key-value pair (default: empty str).
  ([m]
   (map->str m " " " " ""))
  ([m joinVals]
   (map->str m joinVals " " ""))
  ([m joinVals delim]
   (map->str m joinVals " " ""))
  ([m joinVals delim prefix]
   (str (str/join joinVals
                  (map #(str prefix %1 delim %2) (keys m) (vals m)))
        "\n")))
