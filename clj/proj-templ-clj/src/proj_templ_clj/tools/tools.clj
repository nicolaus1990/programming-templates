(ns proj-templ-clj.tools.tools
  (:require ;[jsonista.core :as j]
            [clojure.string :as str]
            ;;Logging:
            [taoensso.timbre :as log])
  (:gen-class))


;; Requires: json to be an object created by jsonista (basically just a map)
(defn debug-json [json msg]
  (log/debug (str msg ": "
                  (str/join "\t"
                            (map #(str %1 ":" (json %1)) (keys json))))))



(defn gen-fn-parse-json-helper [do-something-bookmark]
  ;;Returns a closure with do-something-bookmark that parses
  ;;firefox's json bookmark. Returned fn executes do-something-bookmark
  ;;on every bookmark
   (fn ! [json];Functions name: "!".
   ;If json is array
     (if (vector? json)
       (doseq [bookmark json]
         ;If elem is not a folder
         ;Not tail-recursive (can't use recur).
         (! bookmark))
       ;else: process non-array json
       ;If json is a folder
       (if (json "children")
         (recur (json "children"))
         ;If it's a bookmark 
         (if (and (= (json "type") "text/x-moz-place"))
           ;Store bookmark
           (do-something-bookmark json)
           ;Else: Print it! (If it's not a folder, nor a bookmark, what is it?)
           (debug-json json "Unsupported json-type"))))))

(comment ;;Requires metosin/jsonista 0.2.2
  (defn parse-json
    ([jFile do-something-bookmark]
     (let [file (java.io.File. jFile)
           json (j/read-value file)
                                        ;Initialize closure: parse-json-helper
           parse-json-helper (gen-fn-parse-json-helper do-something-bookmark)]
       (parse-json-helper json)))))


