(ns proj-templ-clj.tools.db
  ;(:require [clojure.java.jdbc :as jdbc])
  )

(comment

;;;;;;;;;;;;;;;;;;
  ;; Sqlite-specific
;;;;;;;;;;;;;;;;;;

  (defn spec-sqlite [pathToDB]
    {:classname   "org.sqlite.JDBC"
     :subprotocol "sqlite"
     :subname     pathToDB})

  (defn query [spec sqlExp]
    (jdbc/query spec [sqlExp]))

  (defn exec-sqlite-query [db-url sqlExp]
    (query (spec-sqlite db-url) sqlExp))

  ;; Gets last generated rowid from given resultset.
  ;; res: The result of a jdbc/insert (lazy sequence)
  ;; Sqlite specific (Postgres handles it differently
  (defn getGenId [res]
    ((first (doall res))
                                        ;Using "keyword" necessary because keyword 'last_inse..' has parenthesis.
     (keyword "last_insert_rowid()")))
  )
